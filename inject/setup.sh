#!/bin/bash -x

### generate a key pair
[[ -f /host/sshkey ]] \
    || ssh-keygen -t rsa -f /host/sshkey -q -N ''

### copy public key to authorized_keys
mkdir -p /root/.ssh
chmod 700 /root/.ssh
cat /host/sshkey.pub > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

### customize the configuration of sshd
sed -i /etc/ssh/sshd_config \
    -e '/PermitRootLogin/ c PermitRootLogin without-password'
systemctl restart sshd
