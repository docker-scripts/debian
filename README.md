# Minimal Debian container

## Installation

  - First install `ds`: https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull debian`

  - Initialize a directory: `ds init debian @debian1`

  - Fix the settings: `cd /var/ds/debian1/ ; vim settings.sh`

  - Make the container: `ds make`

## Other commands

```
ds stop
ds start
ds shell
ds help
```
